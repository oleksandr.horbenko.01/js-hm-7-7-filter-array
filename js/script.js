// 1. Метод forEach виводить в консоль кожен елемент масиву,
// застосовується до кожного елемента масиву
// 2.Очищаємо масив за допомогою length. Array.length = 0
// 3.Щоб перевірити чи зміна є масивом, треба використати Array.isArray(arr)


let filterBy = function(arr, dataType) {
    // let result = [];

    let newArray = arr.filter((elem) => {
        if(typeof elem  === dataType ){
            return false;
        } else {
            return true;
        }
    });

    console.log(newArray);
};

let list = ["Odessa", "umbrella", 99, "99", false, 100];

filterBy(list, "string");
filterBy(list, "number");
filterBy(list, "boolean")



